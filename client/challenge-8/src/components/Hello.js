import React from 'react';

function Hello(props) {
    const { count } = props

    return(
        <div>
            <h1>Hello {count} </h1>
        </div>
    )
}

export default Hello;