import React, { Component } from 'react'

class Toggle extends Component {
    constructor(props) {
      super(props);
      this.state = {
          todoText: '',
          items: [],
          counter: 0
      };
    }

    handleSubmit = () => {
        var itemsTodo = this.state.items
        itemsTodo.push(this.state.todoText)
        this.setState({
            items: itemsTodo
        })
    }

    handleInput = (e) => {
        // console.log('input dari user: ' + e.target.value)
        this.setState({
            todoText: e.target.value
        })
    }

    handleCount = () => {
        let newCounter = this.state.counter + 1;
        this.setState({
            counter: newCounter
        })
    }

    render() {
      return (
          <div>
              <h1>Binar TodoList</h1>

              <form>
                  <input type="text" name="binar-todo"
                  placeholder='Apa yang akan kamu lakukan hari ini?'
                  onChange={this.handleInput} />
              </form>
              <h1>{this.state.counter}</h1>
              <button onClick={this.handleCount}> Count </button>
          </div>
      );
    }
}

export default Toggle;