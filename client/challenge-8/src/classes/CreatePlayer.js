import React, { Component } from 'react'

// import './App.css';

class CreatePlayer extends Component {
    constructor(props) {
        super(props);
        this.state = {
            playerList: [],
            username: "",
            email: "",
            password: ""
        }
        console.log(this.state)
    }

    handleInputChange = (e) => {
        const name = e.target.name
        const value = e.target.value
        this.setState({
            [name]: value
        })
        
    }

    handleSubmit = (e) => {
        let newPlayerList = this.state.playerList
        newPlayerList.push({
            username: this.state.username,
            email: this.state.email,
            password: this.state.password
        })
        this.setState({
            playerList: newPlayerList,
            username: "",
            email: "",
            password: ""
        })
    }

    tambahan = () => {
        <div>Tambahan</div>
    }

    render() {
        console.log(this.state)
        return(
            <div>
              <h1>Create Player</h1>

              <form>
                  <label>Username: </label>
                  <input type="text" name="username" placeholder='Insert your username'
                  onChange={this.handleInputChange}/> <br/>
                  <label>Email: </label>
                  <input type="text" name="email" placeholder='Email address'
                  onChange={this.handleInputChange}/> <br/>
                  <label>Password: </label>
                  <input type="text" name="password" placeholder='Insert your password '
                  onChange={this.handleInputChange}/> <br/>
                  <input type="button" onClick={this.handleSubmit} value="Submit"></input>
              </form>
              <br/>
              <table className="table">
                  <thead>
                      <tr>
                          <th>Username</th>
                          <th>Email</th>
                          <th>Password</th>
                      </tr>
                  </thead>
                  <tbody>
                      { this.state.playerList.map(player => (
                        <tr>
                            <td>{player.username}</td>
                            <td>{player.email}</td>
                            <td>{player.password}</td>
                        </tr>
                      )) }
                  </tbody>
              </table>
          </div>
        )
    }
}

export default CreatePlayer;