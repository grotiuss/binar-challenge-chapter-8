import React, { Component } from 'react'

import logo from './logo.svg';
import './App.css';

//Components
import Hello from './components/Hello'
import HelloTeman from './components/HelloTeman'

//Classes
import Toggle from './classes/Toggle'
import CreatePlayer from './classes/CreatePlayer'

class App extends Component {
  constructor() {
    super();
    this.state = {
      tab: 'home',
      name: "Grotius Cendikia Hasiholan",
      counter: 0
    }
  }

  render() {
    return (
      // <div className="App">
      //       {/* <header className="App-header">
      //         <Hello count = "0"/>
      //         <HelloTeman />
      //       </header> */}
      // </div>
      <CreatePlayer />
    );
  }
}

export default App;
